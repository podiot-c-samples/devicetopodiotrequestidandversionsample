/*
   this sketch is a sample of how to device must connect to podiot.
   in this sample, device send values of moisture sensore and ipaddress every 1 minute to podiot, and turn on/off
   a waterpump base on message received from user via podiot.

*/

#include <WiFi.h>       // Core WiFi Library
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h> //version 6.x

#define moisture_Sensor A0 // moisture Sensor Pin
#define waterpump_Relay D2  // waterpump relay Pin

int moistureValue;

const char *ssid = "Your SSID";
const char *password = "Your Password";

String reportedRequestId;
String getRequestId;

bool allowToStart = false;
bool nextReport = false;
bool allowPublish = false;    //flag for permitted to publish
bool CheckConnection = false; //flag for check MQTT connection & WiFi Connection
bool allowGetPublish = false;
bool timeOut = false;
bool firstPublish = false;
long desiredVersion;
long desiredVersion_last;

long reportedVersion;
long reportedVersion_last;

volatile int Counter = 0; //use Volatile before variable due to removed by compiler optimizations

char *letters = "abcdefghijklmnopqrstuvwxyz0123456789";

//String message = ""; //for send reported data to Podiot

const char *mqtt_server = "iot-mqtt.pod.ir";            //Podiot Mqtt Broker Address
String clientId = "Your clientId";              //must change to your clientId
String deviceId = "Your deviceId"; //must change to your deviceId

const char *pubTopic_reported = "dvcasy/twin/update/reported"; //Topic for send reported message to podiot
const char *pubTopic_get = "dvcasy/twin/get";             //Topic for get device Twin from podiot
const char *pubTopic_delete = "dvcasy/twin/delete";       //Topic for delete device Twin from podiot

String subTopic_origin;   //for received any message from podiot
String subTopic_desired;  //for received desired message from podiot
String subTopic_document; //for received device twin Document from podiot
String subTopic_accepted; //for received accept message from podiot
String subTopic_rejected; //for received reject message from podiot
WiFiClient espClient;
PubSubClient mqttclient(espClient);

/**
 * create a hardware timer 
 */
hw_timer_t *timer = NULL;

void connectToWiFi()
{
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("MAC address: ");
    Serial.println(WiFi.macAddress());
}
/**
 * @brief use timer to publish data and check connection
 */
void IRAM_ATTR onTimer()
{
    CheckConnection = true;

    if (timeOut == true)
    {
        allowGetPublish = true;
        timeOut = false;
    }
    Counter++;
    if (Counter == 6 )
    {
        if(allowToStart == true){
        Counter = 0;
        timeOut = true;
        allowPublish = true;
        }
        else
        {
        Serial.println("First Publish");
        Counter = 0;
        firstPublish = true;     
       }
    }

}



/**
 * @brief initialize timer for use instesd delay 
 */
void initTimer()
{
  /**
  * Use 1st timer of 4 
  * 1 tick take 1/(80MHZ/80) = 1us so we set divider 80 and count up
  */
    timer = timerBegin(0, 80, true);

  /** 
  * Attach onTimer function to our timer
  */
    timerAttachInterrupt(timer, &onTimer, true);

  /**
  * Set alarm to call onTimer function every 10 second 
  * Repeat the alarm (third parameter) 
  */
    timerAlarmWrite(timer, 10000000, true);

  /**
  * Start an alarm
  */
    timerAlarmEnable(timer);
    Serial.println("start timer");
}

void reconnect()
{
    // Loop until we're reconnected
    while (!mqttclient.connected())
    {
        Serial.println();
        Serial.print(">Attempting MQTT connection...");
        Serial.println();
        // Attempt to connect
    if (mqttclient.connect((char *)clientId.c_str()))        
    {
            Serial.println(">connected");
        }
        else
        {
            Serial.print(">failed, rc=");
            Serial.print(mqttclient.state());
            Serial.println("try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
        Serial.println((char *)subTopic_origin.c_str());
        if (mqttclient.subscribe((char *)subTopic_origin.c_str()))
        {
            Serial.println();
            Serial.println(">subscribed to topic:");
            Serial.println((char *)subTopic_origin.c_str());
        }
        else
        {
            Serial.println();
            Serial.print(">failed to subscribe");
        }
    }
}
void callback(char *topic, byte *payload, unsigned int length)
{
    Serial.print("Message arrived [");
    Serial.print(String(topic).c_str());
    Serial.print("] ");
    Serial.println("");
    
    DynamicJsonDocument doc(1024);
    deserializeJson(doc, payload, length);
    const char *mainMsg = doc["content"];
    Serial.print("\ncontent : ");
    Serial.println(mainMsg);
    DynamicJsonDocument mainDoc(1024);
    deserializeJson(mainDoc, mainMsg);

    if (!strcmp(topic, (char *)subTopic_desired.c_str()))
    {
        Serial.print("\nMessage Received from DesiredTopic:");
        serializeJson(mainDoc, Serial);
        desiredVersion = mainDoc["deviceTwinDocument"]["attributes"]["desired"]["$version"];
        Serial.print("\version of desired topic : ");
        Serial.println(desiredVersion);

        if (desiredVersion > desiredVersion_last)
        {
            Serial.println("\nOk! this message is latest update desired, now update device state base it on!");
            desiredVersion_last = desiredVersion;
            char waterpump[10];
            String command;
            strcpy(waterpump, mainDoc["deviceTwinDocument"]["attributes"]["desired"]["waterpump"]);
            Serial.println("waterpump command is:");
            Serial.println(waterpump);
            command = waterpump;

            if (command == "on")
            {
                Serial.println("turn waterpump on");
                digitalWrite(waterpump_Relay, 1);
            }
            if (command == "off")
            {
                Serial.println("turn waterpump off");
                digitalWrite(waterpump_Relay, 0);
            }
        }
        else
        {
            Serial.println("\nNok! this message not latest update desired, so can decide not to act on a received message!");
        }
    }

    if (!strcmp(topic, (char *)subTopic_document.c_str()))
    {
        Serial.print("\nMessage Received from DocumentTopic:");
        serializeJson(mainDoc, Serial);
    }

    if (!strcmp(topic, (char *)subTopic_accepted.c_str()))
    {
        Serial.print("\nMessage Received from AcceptedTopic:");
        serializeJson(mainDoc, Serial);

        if (mainDoc["$requestId"].as<String>() == "GetForRetrieve$versions")
        {
            desiredVersion_last = mainDoc["deviceTwinDocument"]["attributes"]["desired"]["$version"];
            Serial.print("\ndesiredVersion_last : ");
            Serial.println(desiredVersion_last);

            reportedVersion_last = mainDoc["deviceTwinDocument"]["attributes"]["reported"]["$version"];
            Serial.print("\nreportedVersion_last : ");
            Serial.println(reportedVersion_last);

            allowToStart = true;
        }
        if (mainDoc["$requestId"].as<String>() == "GetForknowLastReportedVersion")
        {
            timeOut = false;
            reportedVersion = mainDoc["deviceTwinDocument"]["attributes"]["reported"]["$version"];
            Serial.print("\nreportedVersion_last : ");
            Serial.println(reportedVersion_last);

            if (reportedVersion == reportedVersion_last)
            {
                Serial.print("\nReported version doesnt update, lets publish again");
            }

            if (reportedVersion == (reportedVersion_last + 1))
            {
                Serial.print("\nReported version updated but No message received on Accepted Topic");
                reportedVersion_last = reportedVersion;
            }
        }

        if (mainDoc["$requestId"].as<String>() == reportedRequestId)
        {
            Serial.print("\nMessage Received from AcceptedTopic for reported update:");
            serializeJson(doc, Serial);
            timeOut = false;

            Serial.println("\nreported message updated successfully on Podiot, now ready for next reported update");
            reportedVersion = mainDoc["deviceTwinDocument"]["attributes"]["reported"]["$version"];
            if (reportedVersion > reportedVersion_last)
            {
                reportedVersion_last = reportedVersion;
                Serial.print("\nlast reported $version in device twin document after reported update is:");
                serializeJson(doc, Serial);
            }
        }
    }

    if (!strcmp(topic, (char *)subTopic_rejected.c_str()))
    {
        Serial.print("\nMessage Received from RejectedTopic:");
        serializeJson(mainDoc, Serial);
        timeOut = false;
        if (mainDoc["$requestId"].as<String>() == "GetForRetrieve$versions")
        {
            Serial.println("\nDevice-twin not found, so $versions equal to 0");
            reportedVersion_last = 0;
            desiredVersion_last = 0;
            allowToStart = true;
        }
        if (mainDoc["$requestId"].as<String>() == reportedRequestId)
        {
            Serial.println("\nreported message didnt update on Podiot, detect error message for more details, then correct reported message and resend it or cancel");
        }
    }
}
void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    Serial.println();

    pinMode(waterpump_Relay, OUTPUT);

    //Connect to wifi network with ssid,password
    connectToWiFi();

    //Creat Subscribe Topics based on Podiot Requirements
    subTopic_origin = "dvcout/";
    subTopic_origin = subTopic_origin + String(deviceId).c_str() + "/";
    subTopic_origin = subTopic_origin + String(clientId).c_str();
    subTopic_origin = subTopic_origin + "/#";
    // Serial.print("subTopic_origin : ");
    // Serial.print(subTopic_origin);

    subTopic_desired = "dvcout/";
    subTopic_desired = subTopic_desired + String(deviceId).c_str() + "/";
    subTopic_desired = subTopic_desired + String(clientId).c_str();
    subTopic_desired = subTopic_desired + "/twin/update/desired";
    //   Serial.print("subTopic_desired : ");
    //   Serial.print(subTopic_desired);

    subTopic_document = "dvcout/";
    subTopic_document = subTopic_document + String(deviceId).c_str() + "/";
    subTopic_document = subTopic_document + String(clientId).c_str();
    subTopic_document = subTopic_document + "/twin/update/document";
    //   Serial.print("subTopic_document : ");
    //   Serial.print(subTopic_document);

    subTopic_accepted = "dvcout/";
    subTopic_accepted = subTopic_accepted + String(deviceId).c_str() + "/";
    subTopic_accepted = subTopic_accepted + String(clientId).c_str();
    subTopic_accepted = subTopic_accepted + "/twin/response/accepted";
    //   Serial.print("subTopic_accepted : ");
    //   Serial.print(subTopic_accepted);

    subTopic_rejected = "dvcout/";
    subTopic_rejected = subTopic_rejected + String(deviceId).c_str() + "/";
    subTopic_rejected = subTopic_rejected + String(clientId).c_str();
    subTopic_rejected = subTopic_rejected + "/twin/response/rejected";
    //   Serial.print("subTopic_rejected : ");
    //   Serial.print(subTopic_rejected);

    //Mqtt Setup
    mqttclient.setServer(mqtt_server, 1883);
    mqttclient.setCallback(callback);

    reconnect();

    initTimer();            //Initialization Timer to use instead delay!
}



void loop()
{ // put your main code here, to run repeatedly:

    mqttclient.loop();
    if (CheckConnection)
    {
        Serial.println("\nCheck Connection");
        CheckConnection = false;
        if (!mqttclient.connected())
            reconnect();
        if (WiFi.status() != WL_CONNECTED)
            connectToWiFi();
    }
    if (firstPublish)
    { // first requestId for start
        getRequestId = "GetForRetrieve$versions";
        Serial.print("\ngetRequestId: ");
        Serial.println(getRequestId);

        unsigned int strlen = getRequestId.length();
        char jsonCharToFirstPublish[100];

        const size_t capacity = JSON_OBJECT_SIZE(1);
        StaticJsonDocument<256> jsonDocToFirstPublish;
        jsonDocToFirstPublish["$requestId"] = getRequestId;
        serializeJson(jsonDocToFirstPublish, jsonCharToFirstPublish);
        serializeJson(jsonDocToFirstPublish, Serial);
        Serial.println();

        if (mqttclient.publish(pubTopic_get, jsonCharToFirstPublish))
        {
            Serial.println("First message published in gettopic! to get repotrd Version and desired Version ");
        }
            firstPublish = false;
    }

    if (allowGetPublish)
    {
        // requestId for findout last reported version
        getRequestId = "GetForknowLastReportedVersion";
        Serial.print("\nreportedRequestId: ");
        Serial.println(reportedRequestId);

        unsigned int strlen = reportedRequestId.length();
        char jsonCharToGetPublish[100];

        const size_t capacity = JSON_OBJECT_SIZE(1);
        StaticJsonDocument<256> jsonDocToGetPublish;
        jsonDocToGetPublish["$requestId"] = getRequestId;

        serializeJson(jsonDocToGetPublish, jsonCharToGetPublish);
        serializeJson(jsonDocToGetPublish, Serial);

        if (mqttclient.publish(pubTopic_get, jsonCharToGetPublish))
        {
            Serial.println(">message published in gettopic!");
        }
        allowGetPublish = false;
    }

    if (allowPublish)
    {    
        Serial.println("\nAllowPublish");
        allowPublish = false;
        moistureValue = analogRead(moisture_Sensor);
        Serial.println(moistureValue);
        moistureValue = map(moistureValue, 7, 700, 0, 100);
        Serial.print("Moisture : ");
        Serial.print(moistureValue);
        Serial.print("%\n");
        Serial.println();
        // Generate a random String into letters character
        reportedRequestId = "";
        for (int i = 0; i < 10; i++)
        {
            reportedRequestId = reportedRequestId + letters[random(0, 36)];
        }
        Serial.print("\nreportedRequestId : ");
        Serial.println(reportedRequestId);

        StaticJsonDocument<256> jsonDoc;
        jsonDoc["$requestId"] = String(reportedRequestId);
        jsonDoc["deviceTwinDocument"]["attributes"]["reported"]["moisture"] = String(moistureValue);
        jsonDoc["deviceTwinDocument"]["attributes"]["reported"]["ipaddress"] = WiFi.localIP().toString().c_str();
        char jsonToChar[1024];
        serializeJson(jsonDoc, jsonToChar);

        if (mqttclient.publish(pubTopic_reported, jsonToChar))
        {
            Serial.print("\nPublish Data: ");
            Serial.println(jsonToChar);
        }
    }
}